/*global angular */

/**
 * Services that persists and retrieves todos from localStorage or a backend API
 * if available.
 *
 * They both follow the same API, returning promises for all changes to the
 * model.
 */
angular.module('todomvc')
    .factory('todoStorage', function ($http, $injector, url) {
        'use strict';

        // Detect if an API backend is present. If so, return the API module, else
        // hand off the localStorage adapter


        return $http.get(url + "task")
            .then(function () {
                return $injector.get('api');
            }, function () {
                return $injector.get('localStorage');
            });
    })

    .factory('api', function ($resource, url) {
        'use strict';

        var store = {
            todos: [],
            lists: [],
            apiTask: $resource(url + 'task/:id', null,
                {
                    update: {method: 'PUT'}
                }
            ),

            apiList: $resource(url + 'list/:id', null,
                {
                    update: {method: 'PUT'}
                }
            ),

            clearCompleted: function () {
                var originalTodos = store.todos.slice(0);

                var incompleteTodos = store.todos.filter(function (todo) {
                    return !todo.completed;
                });

                angular.copy(incompleteTodos, store.todos);

                return store.apiTask.delete(function () {
                }, function error() {
                    angular.copy(originalTodos, store.todos);
                });
            },

            delete: function (todo) {
                var originalTodos = store.todos.slice(0);

                store.todos.splice(store.todos.indexOf(todo), 1);
                return store.apiTask.delete({id: todo.id},
                    function () {
                    }, function error() {
                        angular.copy(originalTodos, store.todos);
                    });
            },

            deleteList: function (list) {
                var originalLists = store.lists.slice(0);
                store.lists.splice(store.lists.indexOf(list), 1);
                return store.apiList.delete({id: list.pk},
                    function () {
                    }, function error() {
                        angular.copy(originalLists, store.lists);
                    });
            },

            getTask: function () {
                return store.apiTask.query(function (resp) {
                    angular.copy(resp, store.todos);
                });
            },

            getList: function () {
                return store.apiList.query(function (resp) {
                    angular.copy(resp, store.lists);
                });
            },

            insert: function (todo) {
                var originalTodos = store.todos.slice(0);
                return store.apiTask.save(todo,
                    function success(resp) {
                        todo.id = resp.id;
                        store.todos.push(todo);
                    }, function error() {
                        angular.copy(originalTodos, store.todos);
                    })
                    .$promise;
            },

            addList: function (list) {
                return store.apiList.save(list,
                    function success(resp) {
                    }, function error(resp) {
                    })
                    .$promise;
            },

            put: function (todo) {
                return store.apiTask.update({id: todo.id}, todo)
                    .$promise;
            }
        };

        return store;
    })

    .factory('localStorage', function ($q) {
        'use strict';

        var STORAGE_ID = 'todos-angularjs';

        var store = {
            todos: [],

            _getFromLocalStorage: function () {
                return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
            },

            _saveToLocalStorage: function (todos) {
                localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
            },

            clearCompleted: function () {
                var deferred = $q.defer();

                var incompleteTodos = store.todos.filter(function (todo) {
                    return !todo.completed;
                });

                angular.copy(incompleteTodos, store.todos);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            delete: function (todo) {
                var deferred = $q.defer();

                store.todos.splice(store.todos.indexOf(todo), 1);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            get: function () {
                var deferred = $q.defer();

                angular.copy(store._getFromLocalStorage(), store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            insert: function (todo) {
                var deferred = $q.defer();

                store.todos.push(todo);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            put: function (todo, index) {
                var deferred = $q.defer();

                store.todos[index] = todo;

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            }
        };

        return store;
    });
