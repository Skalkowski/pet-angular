/*global angular */

/**
 * The main controller for the app. The controller:
 * - retrieves and persists the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
 */
angular.module('todomvc')
    .controller('TodoCtrl', function TodoCtrl($scope, $routeParams, $filter, store) {
        'use strict';
        var lists = $scope.lists = store.lists;
        var todos = $scope.todos = [];
        $scope.actualList = 0;
        $scope.newTodo = '';
        $scope.editedTodo = null;

        $scope.status = '';

        $scope.$watch('lists', function () {
            if (lists[0]) {
                todos = $scope.todos = lists[$scope.actualList].tasks;
                $scope.remainingCount = $filter('filter')(todos, {completed: false}).length;
                $scope.completedCount = todos.length - $scope.remainingCount;
                $scope.allChecked = !$scope.remainingCount;
            }
        }, true);

        // Monitor the current route for changes and adjust the filter accordingly.
        $scope.changeType = function (status) {

            $scope.status = status;

            if (status === 'active') {
                $scope.statusFilter = {completed: false};
            }
            else if (status === 'completed') {
                $scope.statusFilter = {completed: true};
            }
            else {
                $scope.statusFilter = {};
            }
        };

        $scope.changeList = function (index) {
            $scope.actualList = index;
            todos = $scope.todos = lists[index].tasks;
        };

        $scope.addList = function () {
            var newList = {
                name: $scope.newList
            };
            store.addList(newList)
                .then(function success(resp) {
                    $scope.lists.push(resp);
                    $scope.newList = '';
                })
                .finally(function () {
                    $scope.saving = false;
                });
        };

        $scope.removeList = function (list) {
            store.deleteList(list);
        };

        $scope.addTodo = function () {
            var newTodo = {
                title: $scope.newTodo.trim(),
                completed: false,
                list: lists[$scope.actualList].pk
            };

            if (!newTodo.title) {
                return;
            }
            $scope.todos.push(newTodo);
            $scope.saving = true;
            store.insert(newTodo)
                .then(function success() {
                    $scope.newTodo = '';
                })
                .finally(function () {
                    $scope.saving = false;
                });
        };

        $scope.editTodo = function (todo) {
            $scope.editedTodo = todo;
            // Clone the original todo to restore it on demand.
            $scope.originalTodo = angular.extend({}, todo);
        };

        $scope.saveEdits = function (todo, event) {
            // Blur events are automatically triggered after the form submit event.
            // This does some unfortunate logic handling to prevent saving twice.
            if (event === 'blur' && $scope.saveEvent === 'submit') {
                $scope.saveEvent = null;
                return;
            }

            $scope.saveEvent = event;

            if ($scope.reverted) {
                // Todo edits were reverted-- don't save.
                $scope.reverted = null;
                return;
            }

            todo.title = todo.title.trim();

            if (todo.title === $scope.originalTodo.title) {
                $scope.editedTodo = null;
                return;
            }

            store[todo.title ? 'put' : 'delete'](todo)
                .then(function success() {
                }, function error() {
                    todo.title = $scope.originalTodo.title;
                })
                .finally(function () {
                    $scope.editedTodo = null;
                });
        };

        $scope.revertEdits = function (todo) {

            todos[todos.indexOf(todo)] = $scope.originalTodo;
            $scope.editedTodo = null;
            $scope.originalTodo = null;
            $scope.reverted = true;
        };

        $scope.removeTodo = function (todo) {
            todos.splice(todos.indexOf(todo), 1);
            store.delete(todo);
        };

        $scope.saveTodo = function (todo) {
            store.put(todo);
        };

        $scope.toggleCompleted = function (todo, completed) {
            if (angular.isDefined(completed)) {
                todo.completed = completed;
            }
            store.put(todo, todos.indexOf(todo))
                .then(function success() {
                }, function error() {
                    todo.completed = !todo.completed;
                });
        };

        $scope.clearCompletedTodos = function () {
            store.clearCompleted();
        };

        $scope.markAll = function (completed) {
            todos.forEach(function (todo) {
                if (todo.completed !== completed) {
                    $scope.toggleCompleted(todo, completed);
                }
            });
        };
    });