/**
 * Created by michal on 3/18/16.
 */
describe('Protractor Demo App', function () {
    var newListInput = element(by.model('newList'));

    beforeEach(function () {
        browser.get('http://127.0.0.1:8001/#/');
    });

    it('should check adding new list', function () {
        var name = 'new list';
        var lastElement;
        newListInput.sendKeys(name).sendKeys(protractor.Key.ENTER);

        lastElement = element.all(by.repeater('list in lists')).last();
        expect(lastElement.getText()).toEqual(name);
    });

    it('should remove list', function () {
        var name = 'new list2';
        var ptor = browser.driver;
        var row;
        newListInput.sendKeys(name).sendKeys(protractor.Key.ENTER);

        row = element.all(by.repeater('list in lists')).last();
        ptor.actions().mouseMove(row).perform();
        row.element(by.tagName('button')).click();

        element.all(by.repeater('list in lists')).each(function (list) {
            expect(list.getText()).not.toContain(name);
        });

    });

    it('should add task to list', function () {
        var nameList = 'one task list';
        var nameTask = 'new task';
        var row;
        newListInput.sendKeys(nameList).sendKeys(protractor.Key.ENTER);

        row = element.all(by.repeater('list in lists')).last();
        row.element(by.tagName('a')).click();

        element(by.model('newTodo')).sendKeys(nameTask).sendKeys(protractor.Key.ENTER);

        browser.sleep(1000);

        element.all(by.repeater('todo in todos')).each(function (task) {
            expect(task.getText()).toEqual(nameTask);
        });

    });

    it('should remove task from list', function () {
        var nameList = 'remove task list';
        var nameTask = 'new task';
        var ptor = browser.driver;
        var rowTask, row;

        newListInput.sendKeys(nameList).sendKeys(protractor.Key.ENTER);

        row = element.all(by.repeater('list in lists')).last();
        row.element(by.tagName('a')).click();

        element(by.model('newTodo')).sendKeys(nameTask).sendKeys(protractor.Key.ENTER);

        browser.sleep(1000);

        rowTask = element.all(by.repeater('todo in todos')).last();
        ptor.actions().mouseMove(rowTask).perform();
        rowTask.element(by.tagName('button')).click();

        element.all(by.repeater('todo in todos')).each(function (task) {
            expect(task.getText()).not.toContain(nameTask);
        });

    });

    it('should move task to completed', function () {
        var nameList = 'status task list';
        var nameTask = 'completed task';
        var row, rowTask;
        newListInput.sendKeys(nameList).sendKeys(protractor.Key.ENTER);

        row = element.all(by.repeater('list in lists')).last();
        row.element(by.tagName('a')).click();

        element(by.model('newTodo')).sendKeys(nameTask).sendKeys(protractor.Key.ENTER);

        browser.sleep(1000);


        rowTask = element.all(by.repeater('todo in todos')).last();
        rowTask.element(by.css('.view')).element(by.css('.toggle')).click();


        element(by.id('completedFilter')).click();
        element.all(by.repeater('todo in todos')).each(function (task) {
            expect(task.getText()).toEqual(nameTask);
        });

    });
});